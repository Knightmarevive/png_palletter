#include <iostream>
#include <limits>
#include <cmath>


#include <algorithm>
#include <string>
#include <set>
#include <map>
#include <vector>


#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_color.h>

// struct zRGB{char r,g,b,a;};
typedef ALLEGRO_COLOR zRGB;
// struct zHSL{float h,s,l;};

bool operator==(const zRGB a,const zRGB b){
    return (a.r == b.r && a.g == b.g && a.b == b.b);
}
#include "color_difference.h"

/*
bool operator<(const zRGB a,const zRGB b){
    if(a.r<b.r) return true;
    if(a.g<b.g) return true;
    if(a.b<b.b) return true;
    return false;
}
*/
/*
bool operator<(const zRGB a,const zRGB b){
    return
    ((a.r+a.g<<8+a.b<<16) <
    (b.r+b.g<<8+b.b<<16));
}
const ALLEGRO_COLOR& z_convert(const zRGB& from){
    return al_map_rgb_i(from.r,from.g,from.b);
}
const zRGB& z_convert(const ALLEGRO_COLOR& from){
    zRGB ret; al_unmap_rgb_i(from,&from.r,&from.g,&from.b); return ret;
}
*/



/*
float zRGB_diff(const zRGB &a,const zRGB &b){

    return( 
        double (a.r-b.r)*double (a.r-b.r) +
        double (a.g-b.g)*double (a.g-b.g) +
        double (a.b-b.b)*double (a.b-b.b)
        // double (a.a-b.a)*double (a.a-b.a)
    );
    
    //return std::abs(zRGB2Hue(a)-zRGB2Hue(b));
}
float zHSL_diff(const zHSL &a, const zHSL &b){
    // constexpr double tmp = double(1)/360/360;
    double hdiff = ;
    return( 
        hdiff*hdiff +
        double (a.s-b.s)*double (a.s-b.s) +
        double (a.l-b.l)*double (a.l-b.l)
    );
}
*/
/*
bool operator<(const zRGB a,const zRGB b){
    zHSL c {1.0f,1.0f,1.0f};
    return(zHSL_diff(zRGB2HSL(a),c) - zHSL_diff(zRGB2HSL(b),c) < 0);
}
*/
/*
bool operator<(const zRGB a,const zRGB b){
    zRGB c = al_map_rgb_f(0.0,0.0,0.0);
    return(zRGB_diff(a,c) - zRGB_diff(b,c) < 0);
}
*/

typedef std::set<zRGB> zPAL;
typedef std::map<zRGB, zRGB> XLAT;

void prepare_pallette(ALLEGRO_BITMAP* in, zPAL& out){
    long hlp = 0; 
    long w,h; w=al_get_bitmap_width(in); h=al_get_bitmap_height(in);
    for(long y=0; y<h;++y) for(long x=0; x<w;++x){

        out.insert(al_get_pixel(in,x,y));
        ++hlp; if((hlp & 65535) == 0) std::cout << "."; //debug

    }
    
    std::cout << std::endl;
}

zRGB select_colours(const std::vector<zRGB>& vec, const zRGB& col, long left, long right){
    long hlp = 0; 
    
    /*
    if(left==right) return vec[left];

    long shift = (right - left)/2;
    long middle = left + shift;
    if(vec[middle]<col) return bisect_colours(vec,col, middle, right);
    else                return bisect_colours(vec,col, left  , middle);
    */

    long middle;
    while(left < right - 1){
        long shift = (right - left)/2;
        middle = left + shift;

        if(vec[left] == col) return col;

        if(vec[middle]<col) {
            left = middle;
            //std::cout << "L"; //debug
        } else {
            right = middle;
            //std::cout << "R"; //debug
        }

        ++hlp;  if((hlp & 65535) == 0){
            std::cout << "! "; //debug
            std::cout << left << ":" << right << "_";
            std::cout << vec[left].r <<"-"<< vec[left].g <<"-"<< vec[left].b; 
            std::cout <<":" << vec[right].r << "-"<< vec[right].g << "-"<< vec[right].b;
            
            system("pause");
        }
    }
    
    //// (un)comment this one to change algorithm 
    // return vec[left];

    left  = middle - 0x400; if (left<0) left =0;
    right = middle + 0x400; if (right>vec.size()) right = vec.size();

    zRGB ret = vec[middle];
    //double dist = zRGB_diff_euclidean(ret, col);
    double dist = zRGB_diff_manhattan(ret, col);
    //double dist = Delta_E_CIE(zXYZ2LAB(zRGB2XYZ(ret)),zXYZ2LAB(zRGB2XYZ(col)));
    for(int i=left;i<right;++i){

        //double tmp = zRGB_diff_euclidean(ret, col);
        double tmp = zRGB_diff_manhattan(ret, col);
        // double tmp = Delta_E_CIE(zXYZ2LAB(zRGB2XYZ(ret)),zXYZ2LAB(zRGB2XYZ(col)));
        
        if( tmp < dist)
            {ret = vec[i]; dist = tmp; middle = i;}
    }

    left  = middle - 0x20; if (left<0) left =0;
    right = middle + 0x20; if (right>vec.size()) right = vec.size();
    dist = Delta_E_CIE(zXYZ2LAB(zRGB2XYZ(ret)),zXYZ2LAB(zRGB2XYZ(col)));
    for(int i=left;i<right;++i){
        double tmp = Delta_E_CIE(zXYZ2LAB(zRGB2XYZ(ret)),zXYZ2LAB(zRGB2XYZ(col)));
        
        if( tmp < dist)
            {ret = vec[i]; dist = tmp;}
    }

    return ret;
}

void prepare_translation(zPAL& from, zPAL& to, XLAT& out){
    long hlp = 0; // char marker[512] = "prepare_translation";

    std::vector<zRGB> target;
    for(zRGB item: to) target.push_back(item);

    for(zRGB left: from){
        // double dist = std::numeric_limits<double>::max();
        // zRGB ret = left; //al_map_rgb(255,0,255);

        // if(to.find(left) != to.end()) out[left] = left; else
        {
            /*
            for(zRGB right: to){
                double tmp = zRGB_diff(left, right);
                if( tmp <= dist)
                    {ret = right; dist = tmp;}
            }
            out[left] = ret;
            */ 
           out[left] = select_colours(target,left, 0, target.size());
        }
        ++hlp; if((hlp & 0xFFF) == 0) std::cout << "."; //debug
    }
    std::cout << std::endl;
}

ALLEGRO_BITMAP* translate(ALLEGRO_BITMAP* from, XLAT& how){
    long hlp = 0;
    long w,h; w=al_get_bitmap_width(from); h=al_get_bitmap_height(from);
    ALLEGRO_BITMAP* ret = al_create_bitmap(w,h);
    al_set_target_bitmap(ret);
    for(long y=0; y<h;++y) for(long x=0; x<w;++x){
        ALLEGRO_COLOR from_color = al_get_pixel(from,x,y);
        al_put_pixel(x,y,how[from_color]);
        ++hlp; if((hlp & 65535) == 0) std::cout << "."; //debug
    }
    std::cout << std::endl;
    return ret;
} 

int main(void){
    al_init(); al_init_image_addon();

   // al_set_new_bitmap_format(ALLEGRO_PIXEL_FORMAT_ARGB_8888);
   // al_set_new_bitmap_format(ALLEGRO_PIXEL_FORMAT_ANY);
   al_set_new_bitmap_format(ALLEGRO_PIXEL_FORMAT_ANY_32_NO_ALPHA );
   al_set_new_bitmap_flags(ALLEGRO_MEMORY_BITMAP);
   
   std::cout << "Initialized Allegro" << std::endl;
   
   ALLEGRO_BITMAP* A; ALLEGRO_BITMAP* B; ALLEGRO_BITMAP* C;
   A = al_load_bitmap("A.png"); std::cout << "A.png file loaded" << std::endl;
   B = al_load_bitmap("B.png"); std::cout << "B.png file loaded" << std::endl;
   
   std::cout << "Loaded input files" << std::endl;
   std::cout << "Converting " << std::endl;

    double tm0 = al_get_time();

    zPAL A_PAL, B_PAL, C_PAL; XLAT translation;
    prepare_pallette(A, A_PAL); std::cout << "A.png pallette prepared (" << A_PAL.size() << " colours)" << std::endl;
    prepare_pallette(B, B_PAL); std::cout << "B.png pallette prepared (" << B_PAL.size() << " colours)" << std::endl;
    double tm1 = al_get_time();

    prepare_translation(B_PAL,A_PAL,translation); std::cout << "pallette translator prepared" << std::endl;
    double tm2 = al_get_time();

    C = translate(B,translation); std::cout << "C.png file prepared" << std::endl;
    double tm3 = al_get_time();

    std::cout << "Converted " << std::endl << std::endl;

    std::cout << "Time Statistics: " << std::endl;
    std::cout << "\t Generating pallettes: " << tm1-tm0 << " seconds" << std::endl;
    std::cout << "\t Generating color translating map: " << tm2-tm1 << " seconds" << std::endl;
    std::cout << "\t Active color translation: " << tm3-tm2 << " seconds" << std::endl;
    std::cout << "\t Total algorithm: " << tm3-tm0 << " seconds" << std::endl;
    std::cout << std::endl;

    prepare_pallette(C, C_PAL);    
    std::cout << "C.png would have " << C_PAL.size() <<" colours "<< std::endl;

    al_save_bitmap("C.png",C);

    std::cout << "Saved output files" << std::endl;
    system("pause");
}