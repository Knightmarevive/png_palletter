#pragma once

struct zHSL{float h,s,l;}; // HSL color space
inline zHSL zRGB2HSL(zRGB color){
    static long long hlp = 0; 

    float max = std::max(color.r,std::max(color.g,color.b));
    float min = std::min(color.r,std::min(color.g,color.b));
    float hue = 0.0; float sat = 0.0;
    float lum = (min + max) * 0.5;

    if(min!=max)
    {
        if(color.r == max) 
            hue= ((color.g-color.b)/(max-min));
        else if (color.g  == max)
            hue= (2.0 + (color.b - color.r)/(max-min));
        else hue= (4.0 + (color.r - color.g)/(max-min));  
        hue *= 60.0; if(hue<0) hue+= 360.0;
    }

    if(max != 0)
    {
        if(lum <= 0.5)  sat = (max-min)/(max+min);
        else            sat = ( max-min)/(2.0-max-min);
    }
    
    /*
    if((hlp & 0xFFFFFFF) == 0) {
        std::cout << "color RGB:" << color.r  << " "<< color.g <<" "<<  color.b << std::endl;
        std::cout << "color HSL:" << hue  << " "<< sat <<" "<<  lum << std::endl;
    }
    */

    // zHSL ret; ret.h=hue;ret.s=sat;ret.l=lum;
    ++hlp; return zHSL{hue,sat,lum};
}


struct zXYZ{float x,y,z;}; // XYZ color space
inline zXYZ zRGB2XYZ (zRGB color){
    float var_R = color.r;float var_G = color.g;float var_B = color.b;
    
    if ( var_R > 0.04045 ) var_R = powf(( ( var_R + 0.055 ) / 1.055 ), 2.4);
    else                   var_R = var_R / 12.92;
    if ( var_G > 0.04045 ) var_G = powf(( ( var_G + 0.055 ) / 1.055 ) , 2.4);
    else                   var_G = var_G / 12.92;
    if ( var_B > 0.04045 ) var_B = powf(( ( var_B + 0.055 ) / 1.055 ) , 2.4);
    else                   var_B = var_B / 12.92;

    var_R = var_R * 100; var_G = var_G * 100; var_B = var_B * 100;
    
    zXYZ ret;
    ret.x = var_R * 0.4124 + var_G * 0.3576 + var_B * 0.1805;
    ret.y = var_R * 0.2126 + var_G * 0.7152 + var_B * 0.0722;
    ret.z = var_R * 0.0193 + var_G * 0.1192 + var_B * 0.9505;
    return (ret);
}

struct zLAB{float L,a,b;}; // CIE-L*ab color space
inline zLAB zXYZ2LAB (zXYZ color){
    float var_X = color.x / 95.047; //var_X = X / Reference-X
    float var_Y = color.y / 100.00; //var_Y = Y / Reference-Y
    float var_Z = color.z / 108.883;//var_Z = Z / Reference-Z

    if ( var_X > 0.008856 ) var_X = pow(var_X, ( 1/3 ));
    else                    var_X = ( 7.787 * var_X ) + ( 16 / 116 );
    if ( var_Y > 0.008856 ) var_Y = pow(var_Y, ( 1/3 ));
    else                    var_Y = ( 7.787 * var_Y ) + ( 16 / 116 );
    if ( var_Z > 0.008856 ) var_Z = pow(var_Z, ( 1/3 ));
    else                    var_Z = ( 7.787 * var_Z ) + ( 16 / 116 );

    zLAB ret;
    ret.L = ( 116 * var_Y ) - 16;
    ret.a = 500 * ( var_X - var_Y );
    ret.b = 200 * ( var_Y - var_Z );
    return ret;
}

inline double zRGB_diff_euclidean(const zRGB &a,const zRGB &b){

    return( 
        double (a.r-b.r)*double (a.r-b.r) +
        double (a.g-b.g)*double (a.g-b.g) +
        double (a.b-b.b)*double (a.b-b.b)
        // double (a.a-b.a)*double (a.a-b.a)
    );
    
}

inline float zRGB_diff_euclidean_weighted(const zRGB &a,const zRGB &b){

    return( 
        double (a.r-b.r)*double (a.r-b.r) * 9.0  +
        double (a.g-b.g)*double (a.g-b.g) * 16.0 +
        double (a.b-b.b)*double (a.b-b.b) * 9.0
        // double (a.a-b.a)*double (a.a-b.a)
    );
    
}

inline float zRGB_diff_manhattan(const zRGB &a,const zRGB &b){
    return (3*std::abs(a.r-b.r)+4*std::abs(a.g-b.g)+3*std::abs(a.b-b.b));
}

/*
bool operator<(const zRGB a,const zRGB b){
    // zRGB c = al_map_rgb_f(0.0,0.0,0.0); return(zRGB_diff_euclidean(a,c) - zRGB_diff_euclidean(b,c) < 0);
    // zRGB c = al_map_rgb_f(0.0,0.0,0.0); return(zRGB_diff_manhattan(a,c) - zRGB_diff_manhattan(b,c) < 0);
    zRGB c = al_map_rgb_f(0.0,0.0,0.0); return(zRGB_diff_euclidean_weighted(a,c) - zRGB_diff_euclidean_weighted(b,c) < 0);
}
*/

/*
bool operator<(const zRGB a,const zRGB b){
    zHSL A =zRGB2HSL(a); zHSL B =zRGB2HSL(b); 
    long H1 = A.h *1.0; long H2 = B.h *1.0;
    if(H1 != H2) return (H1<H2);
    
    //long L1 = A.l * 36.0; long L2 = B.l * 36 .0;
    //if(L1!=L2) return L1<L2;
    //return(A.s< B.s);
    
   return (A.l*A.s<B.l*B.s);
}
*/

/*
bool operator<(const zRGB a,const zRGB b){
    zHSL A =zRGB2HSL(a); zHSL B =zRGB2HSL(b); 

    // return( double (A.h-B.h) < 0 );

     return( 
        double (A.h-B.h) * 0.5 +
        double (A.s-B.s) +
        double (A.l-B.l) 
        < 0.0
    );   

}
*/

/*
bool operator<(const zRGB a,const zRGB b){
     zHSL A =zRGB2HSL(a); zHSL B =zRGB2HSL(b); 
     return(A.h < B.h);

    long H1 = A.h *10.0; // H1 %= 360; 
    long H2 = B.h *10.0; // H2 %= 360;
    if(H1 != H2) return (H1<H2);
    return ((a.r-b.r)*3+(a.g-b.g)*4+(a.b-b.b)*3);
}
*/

/*
bool operator<(const zRGB a,const zRGB b){
     zHSL A =zRGB2HSL(a); zHSL B =zRGB2HSL(b);
     constexpr double deg2one = 1.0/360.0;
     double H1 = A.h * deg2one;
     double H2 = B.h * deg2one;

     return (H1*16+A.s*2+A.l*1<H2*16+B.s*2+B.l*1);
     
}
*/

inline float Delta_E_CIE (zLAB a, zLAB b){
     return sqrt(
         (a.L - b.L)*(a.L - b.L)+
         (a.a - b.a)*(a.a - b.a)+
         (a.b - b.b)*(a.b - b.b)

     );
 }

 inline float zLAB_diff (zLAB a, zLAB b){
     return (
         (a.L - b.L)*(a.L - b.L)+
         (a.a - b.a)*(a.a - b.a)+
         (a.b - b.b)*(a.b - b.b)

     );
 }

/*
 bool operator<(const zRGB a,const zRGB b){
     zRGB black{0,0,0,0};
     zLAB C{50.0f,300.0f,0.0f};
     zLAB B=zXYZ2LAB(zRGB2XYZ(b));
     zLAB A=zXYZ2LAB(zRGB2XYZ(a));
     return (zLAB_diff(A,C)*64+zRGB_diff_euclidean_weighted(a,black)<zLAB_diff(B,C)*64+zRGB_diff_euclidean_weighted(b,black));
 }
 */

/*
bool operator<(const zRGB a, const zRGB b){
    long H1 = zRGB2HSL(a).h *50.0; long H2 = zRGB2HSL(b).h *50.0;
    if(H1 != H2) return H1<H2;

    long H1 = zRGB2HSL(a).h *50.0; long H2 = zRGB2HSL(b).h *50.0;
    

     // zLAB C{0.0f,0.0f,0.0f};
     // zLAB B=zXYZ2LAB(zRGB2XYZ(b));
     // zLAB A=zXYZ2LAB(zRGB2XYZ(a));
     // return (zLAB_diff(A,C)<zLAB_diff(B,C));
    
    // return(A.a<B.a);
}
*/


bool operator<(const zRGB a,const zRGB b){
    if(a==b) return false;

     zHSL A =zRGB2HSL(a); 
     zHSL B =zRGB2HSL(b);
     
     if(A.h>330) A.h -= 360.0f; A.h += 30;
     if(B.h>330) B.h -= 360.0f; B.h += 30;

    {
         constexpr float Hx = 8.0/360.0;
         long H1 = A.h * Hx;     
         long H2 = B.h * Hx;
         if(H1 != H2) return H1<H2;

        constexpr float Lx = 8.0f;
        long L1 = A.l * Lx; 
        long L2 = B.l * Lx;
        if(L1 != L2) return L1<L2; 
        //return (A.s < B.s);

        constexpr float Sx = 8.0f;
        long S1 = A.s * Sx; 
        long S2 = B.s * Sx;
        if(S1 != S2) return S1<S2;
        //return (A.l<B.l);
    }

    {
         constexpr float Hx = 24.0/360.0;
         long H1 = A.h * Hx;     
         long H2 = B.h * Hx;
         if(H1 != H2) return H1<H2;

        constexpr float Lx = 24.0f;
        long L1 = A.l * Lx; 
        long L2 = B.l * Lx;
        if(L1 != L2) return L1<L2; 
        //return (A.s < B.s);

        constexpr float Sx = 24.0f;
        long S1 = A.s * Sx; 
        long S2 = B.s * Sx;
        if(S1 != S2) return S1<S2;
        //return (A.l<B.l);


    }

    {
         constexpr float Hx = 72.0/360.0;
         long H1 = A.h * Hx;     
         long H2 = B.h * Hx;
         if(H1 != H2) return H1<H2;

        constexpr float Lx = 16.0f;
        long L1 = A.l * Lx; 
        long L2 = B.l * Lx;
        if(L1 != L2) return L1<L2; 
        //return (A.s < B.s);

        constexpr float Sx = 16.0f;
        long S1 = A.s * Sx; 
        long S2 = B.s * Sx;
        if(S1 != S2) return S1<S2;
        //return (A.l<B.l);
    }


    {
         constexpr float Hx = 1.0;// 72.0/360.0;
         long H1 = A.h * Hx;     
         long H2 = B.h * Hx;
         if(H1 != H2) return H1<H2;

        constexpr float Lx = 64.0f;
        long L1 = A.l * Lx; 
        long L2 = B.l * Lx;
        if(L1 != L2) return L1<L2; 
        //return (A.s < B.s);

        constexpr float Sx = 64.0f;
        long S1 = A.s * Sx; 
        long S2 = B.s * Sx;
        if(S1 != S2) return S1<S2;
        //return (A.l<B.l);
    }


    // return(A.s*5+A.l < B.s*5 + B.s);
    // return(a.r+a.g*1024+a.b*1048576)<(b.r+b.g*1024+b.b*1048576);

    // static zRGB black; 
    /*
    double zDiff = a.r*a.r + a.g*a.g + a.b*a.b - b.r*b.r + b.g*b.g + b.b*b.b;
    constexpr double delta = 0.125;
    if (zDiff > delta || zDiff < -delta) 
        return zDiff < 0;
    */
    //
    
    /*
    double zDiff = A.s*A.s + A.l*A.l - B.s*B.s+B.l*B.l;
    constexpr double delta = 1.0/256.0;
    if (zDiff > delta || zDiff < -delta) 
        return zDiff < 0;
    */

    // return false;
    // std::cout << "!"; 
    // std::cout << A.h <<":"<< A.s <<":"<< A.l;

    long G1 = a.g * 255; long G2 = b.g * 255;
    if(G1!=G2) return G1<G2;
    long R1 = a.r * 255; long R2 = b.r * 255;
    if(R1!=R2) return R1<R2;
    return a.b<b.b;

}


/*
bool operator<(const zRGB a,const zRGB b){
     zHSL A =zRGB2HSL(a); zHSL B =zRGB2HSL(b);
     long H1 = A.h * 0.2f; long H2 = B.h * 0.2f;
     if(H1 != H2) return H1<H2;
     return(A.l*A.l+A.s*A.s *25 < B.l*B.l + B.s *B.s *25);
}
*/

/*
bool operator<(const zRGB a,const zRGB b){
     zHSL A =zRGB2HSL(a); zHSL B =zRGB2HSL(b);
     long H1 = A.h * 0.125f; long H2 = B.h * 0.125f;
     if(H1 != H2) return H1<H2;
     return(A.l*A.l*196+A.s*A.s < B.l*B.l*196 + B.s *B.s);
}
*/

/*
bool operator<(const zRGB a,const zRGB b){
     zHSL A =zRGB2HSL(a); 
     zHSL B =zRGB2HSL(b);
     
     // if(A.h>320) A.h -= 360.0f;
     // if(B.h>320) B.h -= 360.0f;

     long H1 = A.h ; //* 0.375f;     
     long H2 = B.h ; //* 0.375f;
     if(H1 != H2) return H1<H2;
     return A.s < B.s;

     // return (3*a.r+4*a.g+3*a.b < 3*b.r+4*b.g+3*b.b);
}
*/